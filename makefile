CC=gcc
CFLAGS=-g
RM=rm

all: hub station

clean:
	$(RM) -f *.o goodhub goodstation

hub: hub.c utils.c
	$(CC) $(CFLAGS) -lnsl hub.c utils.c -o goodhub

station: station.c dvrp.c utils.c
	$(CC) $(CFLAGS) -lnsl station.c dvrp.c utils.c -o goodstation

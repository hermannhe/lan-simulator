
# Project
	Local Area Network(LAN) Simulator
	Computer Networks Project 3
	Chenghu He

# Operating System
	Linux
	Ubuntu 14.04
	gcc 4.8.4

# How to run

** Make Programs
	$ make

** Start a Hub
	$ ./goodhub [NAME]

** Start a Station
	$ ./goodstation [NAME]

